//
//  Attributes.swift
//
//  Created by manish on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Attributes {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lastName = "last-name"
    static let firstName = "first-name"
    static let emailSubscriptionStatus = "email-subscription-status"
    static let emailAddress = "email-address"
    static let contactNumber = "contact-number"
    static let title = "title"
    static let emailAddressVerified = "email-address-verified"
    static let paymentType = "payment-type"
    static let dateOfBirth = "date-of-birth"
  }

  // MARK: Properties
  public var lastName: String?
  public var firstName: String?
  public var emailSubscriptionStatus: Bool? = false
  public var emailAddress: String?
  public var contactNumber: String?
  public var title: String?
  public var emailAddressVerified: Bool? = false
  public var paymentType: String?
  public var dateOfBirth: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    lastName = json[SerializationKeys.lastName].string
    firstName = json[SerializationKeys.firstName].string
    emailSubscriptionStatus = json[SerializationKeys.emailSubscriptionStatus].boolValue
    emailAddress = json[SerializationKeys.emailAddress].string
    contactNumber = json[SerializationKeys.contactNumber].string
    title = json[SerializationKeys.title].string
    emailAddressVerified = json[SerializationKeys.emailAddressVerified].boolValue
    paymentType = json[SerializationKeys.paymentType].string
    dateOfBirth = json[SerializationKeys.dateOfBirth].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    dictionary[SerializationKeys.emailSubscriptionStatus] = emailSubscriptionStatus
    if let value = emailAddress { dictionary[SerializationKeys.emailAddress] = value }
    if let value = contactNumber { dictionary[SerializationKeys.contactNumber] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    dictionary[SerializationKeys.emailAddressVerified] = emailAddressVerified
    if let value = paymentType { dictionary[SerializationKeys.paymentType] = value }
    if let value = dateOfBirth { dictionary[SerializationKeys.dateOfBirth] = value }
    return dictionary
  }

}
