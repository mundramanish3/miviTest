//
//  Data.swift
//
//  Created by manish on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Data {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let links = "links"
    static let attributes = "attributes"
    static let id = "id"
    static let type = "type"
    static let relationships = "relationships"
  }

  // MARK: Properties
  public var links: Links?
  public var attributes: Attributes?
  public var id: String?
  public var type: String?
  public var relationships: Relationships?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    links = Links(json: json[SerializationKeys.links])
    attributes = Attributes(json: json[SerializationKeys.attributes])
    id = json[SerializationKeys.id].string
    type = json[SerializationKeys.type].string
    relationships = Relationships(json: json[SerializationKeys.relationships])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = links { dictionary[SerializationKeys.links] = value.dictionaryRepresentation() }
    if let value = attributes { dictionary[SerializationKeys.attributes] = value.dictionaryRepresentation() }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = relationships { dictionary[SerializationKeys.relationships] = value.dictionaryRepresentation() }
    return dictionary
  }

}
