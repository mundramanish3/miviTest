//
//  Attributes2.swift
//
//  Created by manish on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Attributes2 {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let expiryDate = "expiry-date"
    static let primarySubscription = "primary-subscription"
    static let includedDataBalance = "included-data-balance"
    static let autoRenewal = "auto-renewal"
  }

  // MARK: Properties
  public var expiryDate: String?
  public var primarySubscription: Bool? = false
  public var includedDataBalance: Int?
  public var autoRenewal: Bool? = false

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    expiryDate = json[SerializationKeys.expiryDate].string
    primarySubscription = json[SerializationKeys.primarySubscription].boolValue
    includedDataBalance = json[SerializationKeys.includedDataBalance].int
    autoRenewal = json[SerializationKeys.autoRenewal].boolValue
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = expiryDate { dictionary[SerializationKeys.expiryDate] = value }
    dictionary[SerializationKeys.primarySubscription] = primarySubscription
    if let value = includedDataBalance { dictionary[SerializationKeys.includedDataBalance] = value }
    dictionary[SerializationKeys.autoRenewal] = autoRenewal
    return dictionary
  }

}
