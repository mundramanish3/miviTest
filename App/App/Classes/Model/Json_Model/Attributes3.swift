//
//  Attributes3.swift
//
//  Created by manish on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Attributes3 {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let unlimitedText = "unlimited-text"
    static let name = "name"
    static let unlimitedInternationalTalk = "unlimited-international-talk"
    static let unlimitedTalk = "unlimited-talk"
    static let unlimitedInternationalText = "unlimited-international-text"
    static let price = "price"
  }

  // MARK: Properties
  public var unlimitedText: Bool? = false
  public var name: String?
  public var unlimitedInternationalTalk: Bool? = false
  public var unlimitedTalk: Bool? = false
  public var unlimitedInternationalText: Bool? = false
  public var price: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    unlimitedText = json[SerializationKeys.unlimitedText].boolValue
    name = json[SerializationKeys.name].string
    unlimitedInternationalTalk = json[SerializationKeys.unlimitedInternationalTalk].boolValue
    unlimitedTalk = json[SerializationKeys.unlimitedTalk].boolValue
    unlimitedInternationalText = json[SerializationKeys.unlimitedInternationalText].boolValue
    price = json[SerializationKeys.price].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.unlimitedText] = unlimitedText
    if let value = name { dictionary[SerializationKeys.name] = value }
    dictionary[SerializationKeys.unlimitedInternationalTalk] = unlimitedInternationalTalk
    dictionary[SerializationKeys.unlimitedTalk] = unlimitedTalk
    dictionary[SerializationKeys.unlimitedInternationalText] = unlimitedInternationalText
    if let value = price { dictionary[SerializationKeys.price] = value }
    return dictionary
  }

}
