//
//  Attributes1.swift
//
//  Created by manish on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Attributes1 {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let credit = "credit"
    static let dataUsageThreshold = "data-usage-threshold"
    static let creditExpiry = "credit-expiry"
    static let msn = "msn"
  }

  // MARK: Properties
  public var credit: Int?
  public var dataUsageThreshold: Bool? = false
  public var creditExpiry: String?
  public var msn: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    credit = json[SerializationKeys.credit].int
    dataUsageThreshold = json[SerializationKeys.dataUsageThreshold].boolValue
    creditExpiry = json[SerializationKeys.creditExpiry].string
    msn = json[SerializationKeys.msn].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = credit { dictionary[SerializationKeys.credit] = value }
    dictionary[SerializationKeys.dataUsageThreshold] = dataUsageThreshold
    if let value = creditExpiry { dictionary[SerializationKeys.creditExpiry] = value }
    if let value = msn { dictionary[SerializationKeys.msn] = value }
    return dictionary
  }

}
