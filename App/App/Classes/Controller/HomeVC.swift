//
//  ViewController.swift
//  App
//
//  Created by manish on 07/05/18.
//  Copyright © 2018 manish. All rights reserved.
//

import UIKit
import SwiftyJSON

let cellIdentifire = "ProductCell"
class HomeVC: UIViewController {

    var baseResult :BaseClass!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupUI()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension HomeVC {
    
    private func setupUI() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 168.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let aCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifire) as! HomeCell
        
        aCell.lblProductName.text = baseResult.included![2].attributes3?.name
        aCell.lblSubscription.text  = "\((baseResult.included![1].attributes2?.includedDataBalance)! / 1024)"
        aCell.lblPaymentType.text  = baseResult.data?.attributes?.paymentType
        aCell.lblName.text  = (baseResult.data?.attributes?.title)! + " " +  (baseResult.data?.attributes?.firstName)! + " " + (baseResult.data?.attributes?.lastName)!
        return aCell
    }
}


class HomeCell : UITableViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblSubscription: UILabel!
    @IBOutlet weak var  lblPrice: UILabel!
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    
}
