//
//  LoginVC.swift
//  App
//
//  Created by manish on 07/05/18.
//  Copyright © 2018 manish. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginVC: UIViewController {

    //MARK: Variables and Constants
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var btnSignIn: UIButton!
    
    var aBaseResult :BaseClass!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnSignInClick(_ sender: UIButton) {
        
        if txtEmail.text?.isEmpty == true {
            alertShow(message: BlankEmail, is_error: true)
        }else if (!MMUtility().isValidEmail(text: txtEmail.text!)) {
            alertShow(message: CorrectFoormat, is_error: true)
        }
        else{
            if aBaseResult.data?.attributes?.emailAddress == txtEmail.text {
                alertShow(message: ExistEmail, is_error: false)
                
            }else{
                alertShow(message: NotExistEmail, is_error: true)
            }
        }
    }
}

extension LoginVC {
    //MARK: Setup UI
    private func setupUI() {
        
        self.navigationController?.navigationBar.isHidden = true
        btnSignIn.layer.borderColor = UIColor.init(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0).cgColor
        MMUtility().textFieldVLView(textfield: txtEmail)
    
        // Get Data from json local file
        getData()
    }
    
    //MARK: Get data form json file
    private func getData() {
        getDataFromJSOJNFile(path: Bundle.main.path(forResource: "collection", ofType: "json")!)
    }
    
    //MARK: Formatted response
    private func getDataFromJSOJNFile(path: String) {
        
        if let jsonString = try? String(contentsOfFile: path, encoding: String.Encoding.utf8) {
            let jsonParsing = JSON(parseJSON: jsonString)
            print(jsonParsing)
            aBaseResult = BaseClass(json: jsonParsing)
            print(aBaseResult.data?.attributes?.emailAddress ?? "")
        }
        
    }

    private func alertShow(message: String, is_error: Bool) {
        let aTitle = "Mivi"
        let aAlert = UIAlertController(title: aTitle, message: message, preferredStyle: .alert)
        let aOkAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            if is_error == false {
                let aHomeVCObj = MMUtility().storyboardMain.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                aHomeVCObj.baseResult = self.aBaseResult
                self.navigationController?.pushViewController(aHomeVCObj, animated: true)
            }
        })
        
        aAlert.addAction(aOkAction)
        self.present(aAlert, animated: true, completion: {
        })

    }
}
