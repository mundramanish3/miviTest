//
//  MMUtility.swift
//  App
//
//  Created by manish on 07/05/18.
//  Copyright © 2018 manish. All rights reserved.
//

import UIKit

let BlankEmail: String = "Email should not blank."
let CorrectFoormat: String  = "Email is not in correct format."
let ExistEmail: String =  "Email is exist in collection json file."
let NotExistEmail: String = "Email is not exist in collection json file."
  
class MMUtility: NSObject {

    static let sharedinstance = MMUtility()
    
    //MARK: Get Main storyboard reference
    let storyboardMain: UIStoryboard = {
        let aStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        return aStoryboard
    }()
    
    //MARK: Set Left and Right view in textfield
    func textFieldVLView(textfield: UITextField) {
        
        textfield.layer.cornerRadius = textfield.frame.size.height / 2
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.layer.borderWidth = 1.0
        
        let paddingLeftView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 7, height: textfield.frame.height))
        textfield.leftView = paddingLeftView
        textfield.leftViewMode = .always
        
        let paddingRightView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 7, height: textfield.frame.height))
        textfield.rightView = paddingRightView
        textfield.rightViewMode = .always
    }
    
    func isValidEmail(text:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
}
